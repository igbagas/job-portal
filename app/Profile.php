<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //Allow mass assignment
    protected $guarded = [];
}
