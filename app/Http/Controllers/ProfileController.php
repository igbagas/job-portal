<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;

class ProfileController extends Controller
{
    //index profile
    public function index(){
        return view('profile.index');
    }

    //update profile
    public function update(Request $request){

        //validate the request
        $this->validate($request, [
            'address' => 'required',
            'bio' => 'required|max:50',
            'exp' => 'required|max:50',
            'phone_number' => 'required|min:12|numeric'
        ]);

        $user_id = auth()->user()->id;
        Profile::where('user_id', $user_id)->update([
            'address' => request('address'),
            'exp' => request('exp'),
            'bio' => request('bio'),
            'phone_number' => request('phone_number')
        ]);

        return redirect()->back()->with('message', 'Profile Has Been Updated');
    }

    //cover letter
    public function cover(Request $request){
        //validate the cover_letter
        $this->validate($request, [
            'cover_letter' => 'required|mimes:pdf,doc,docx|max:20000'
        ]);

        $user_id = auth()->user()->id;
        $cover = $request->file('cover_letter')->store('public/files');
        Profile::where('user_id', $user_id)->update([
            'cover_letter' => $cover
        ]);

        return redirect()->back()->with('message', 'The Cover Letter Has Been Saved');
    }


    //resume
    public function resume(Request $request){
        //validate the resume
        $this->validate($request, [
            'resume' => 'required|mimes:pdf,doc,docx|max:20000'
        ]);

        $user_id = auth()->user()->id;
        $resume = $request->file('resume')->store('public/files');
        Profile::where('user_id', $user_id)->update([
            'resume' => $resume
        ]);

        return redirect()->back()->with('message', 'The Resume Has Been Saved');
    }

    //avatar
    //use Request $request because its a request update from user
    public function avatar(Request $request){
        //validate the avatar
        $this->validate($request, [
            'avatar' => 'required|mimes:png,jpeg,jpg|max:20000'
        ]);

        $user_id = auth()->user()->id; //get logged in user id
        if($request->hasfile('avatar')){ // check if the request has a file
            $file = $request->file('avatar'); //get the file
            $ext = $file->getClientOriginalExtension(); //get the file original extension
            $filename = time().'.'.$ext;  //give the file name based on time it uploaded and the extension
            $file->move('uploads/avatar', $filename); //move the file to public/uploads/avatar directory
            Profile::where('user_id', $user_id)->update([ //update the profile based on logged in user
                'avatar' => $filename
            ]);

            return redirect()->back()->with('message', 'The Avatar Has Been Updated'); //return the success message
        }
    }

}
