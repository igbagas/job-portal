<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $fillable = [
        'company_nm', 'user_id', 'slug', 'address', 'phone', 'website', 'logo',
        'cover_photo', 'slogan', 'desc'
    ];

    //Has many relation with job
    public function jobs(){
        return $this->hasMany('App\Job');
    }

    public function getRouteKeyName(){
        return 'slug';
    }
}
