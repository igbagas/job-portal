@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            @if(empty(Auth::user()->profile->avatar))
            <img src="{{asset('img/avatar/serwman1.jpg')}}" alt="User Img"
            style="width:100%;">
            @else
            <img src="{{asset('uploads/avatar')}}/{{Auth::user()->profile->avatar}}" alt="User Img"
            style="width:100%;">
            @endif
            <br><br>
            <form action="{{route('avatar.update')}}" method="POST" enctype="multipart/form-data">
            @csrf
                <div class="card">
                    <div class="card-header">
                        Update Avatar
                    </div>
                    <div class="card-body">
                        <input type="file" class="form-control" name="avatar">
                        <br>
                        <button class="btn btn-success float-right" type="submit">
                            Update
                        </button>
                    </div>
                    @if($errors->has('avatar'))
                        <div class="error" style="color: red;">
                            {{$errors->first('avatar')}}
                        </div>
                    @endif
                </div>
            </form>

        </div> <!--col md 3 -->

        <div class="col-md-5">
            @if(Session::has('message'))
                <div class="alert alert-success">
                    {{Session::get('message')}}
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    Update Your Profile
                </div>
                <form action="{{ route('profile.update') }}" method="POST">
                    @csrf

                    <div class="card-body">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" name="address"
                            value="{{Auth::user()->profile->address}}">
                            @if($errors->has('address'))
                                <div class="error" style="color: red;">
                                    {{$errors->first('address')}}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="phone_number">Phone Number</label>
                            <input type="text" class="form-control" name="phone_number"
                            value="{{Auth::user()->profile->phone_number}}">
                            @if($errors->has('phone_number'))
                                <div class="error" style="color: red;">
                                    {{$errors->first('phone_number')}}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="exp">Experience</label>
                            <textarea name="exp" id="exp" class="form-control" cols="30" rows="10">{{Auth::user()->profile->exp}}</textarea>
                            @if($errors->has('exp'))
                                <div class="error" style="color: red;">
                                    {{$errors->first('exp')}}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="bio">Bio</label>
                            <textarea id="bio" class="form-control" name="bio">{{Auth::user()->profile->bio}}</textarea>
                            @if($errors->has('bio'))
                                <div class="error" style="color: red;">
                                    {{$errors->first('bio')}}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success" type="submit">Update</button>
                        </div>
                    </div>

                </form>
            </div>
        </div> <!--col md 5-->

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Your Information
                </div>
                <div class="card-body">
                    <p>Name: {{Auth::user()->name}}</p>
                    <p>Email: {{Auth::user()->email}}</p>
                    <p>Address: {{Auth::user()->profile->address}}</p>
                    <p>Phone Number: {{Auth::user()->profile->phone_number}}</p>
                    <p>Gender : {{Auth::user()->profile->gender}}</p>
                    <p>Experience : {{Auth::user()->profile->exp}}</p>
                    <p>Bio : {{Auth::user()->profile->bio}}</p>
                    <p>Member Since : {{date('d F Y', strtotime(Auth::user()->created_at))}}</p>

                    @if(!empty(Auth::user()->profile->cover_letter))
                        <p><a href="{{Storage::url(Auth::user()->profile->cover_letter)}}">CV</a></p>
                    @else
                        <p>Please upload cover letter</p>
                    @endif


                    @if(!empty(Auth::user()->profile->resume))
                        <p><a href="{{Storage::url(Auth::user()->profile->resume)}}">Resume</a></p>
                    @else
                        <p>Please upload resume</p>
                    @endif
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Update Coverletter
                </div>
                <form action="{{route('profile.cover')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <input type="file" class="form-control" name="cover_letter"><br>
                        <button class="btn btn-success btn-sm float-right" type="submit">Update</button>
                        @if($errors->has('cover_letter'))
                            <div class="error" style="color: red;">
                                {{$errors->first('cover_letter')}}
                            </div>
                        @endif
                    </div>
                    <br>
                </form>
            </div>

            <div class="card">
                <div class="card-header">
                    Update Resume
                </div>
                <form action="{{route('profile.resume')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <input type="file" class="form-control" name="resume"><br>
                        <button class="btn btn-success btn-sm float-right" type="submit">Update</button>
                        @if($errors->has('resume'))
                            <div class="error" style="color: red;">
                                {{$errors->first('resume')}}
                            </div>
                        @endif
                    </div>
                    <br>
                </form>
            </div>
        </div> <!--col md 4-->

    </div>
</div>
@endsection
