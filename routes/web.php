<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'JobController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/jobs/{id}/{job}', 'JobController@show')->name('jobs.show');

Route::get('/company/{id}/{company}', 'CompanyController@index')->name('company.index');

Route::get('/user/profile', 'ProfileController@index');

Route::post('/user/profile/update', 'ProfileController@update')->name('profile.update');

Route::post('/user/profile/coverletter', 'ProfileController@cover')->name('profile.cover');

Route::post('/user/profile/resume', 'ProfileController@resume')->name('profile.resume');

Route::post('/user/profile/avatar', 'ProfileController@avatar')->name('avatar.update');

Route::view('employer/register', 'employer/employer-register');

Route::post('employer/register', 'EmployerController@register')->name('employer.register');


